import { Component, OnInit } from '@angular/core';
import { Ride } from '../ride';
import { RidesService } from '../rides.service';
import { PopoverController} from '@ionic/angular';
import { PopovercomponentPage} from '../popovercomponent/popovercomponent.page'; 
import { AnswerOfferPage } from '../answer-offer/answer-offer.page';
import { AnswerRequestPage } from '../answer-request/answer-request.page';
import { Ride2 } from '../ride2';
import { Rides2Service } from '../rides2.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  offer = Array<Ride>();
  request = Array<Ride2>();
  

  constructor(private popover:PopoverController, public ridesService: RidesService, public rides2Service: Rides2Service) {}

  ngOnInit() {
    this.offer = this.ridesService.offer;
    this.request = this.rides2Service.request;
    
  }

  CreatePopover() {
    this.popover.create({component: PopovercomponentPage,
    showBackdrop:false}).then((popoverElement)=>{
      popoverElement.present();
    });
  }

  CreatePopover2() {
    this.popover.create({component: AnswerOfferPage,
    showBackdrop:false}).then((popoverElement)=>{
      popoverElement.present();
    });
  }

  CreatePopover3() {
    this.popover.create({component: AnswerRequestPage,
    showBackdrop:false}).then((popoverElement)=>{
      popoverElement.present();
    });
  }


}
