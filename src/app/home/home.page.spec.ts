import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import {
  async,
  ComponentFixture,
  TestBed,
  inject,
} from "@angular/core/testing";
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

import { debug } from 'util';

import { By } from '@angular/platform-browser';

import { HttpClientModule } from "@angular/common/http";

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let debugEl: DebugElement;
  let htmlEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("the home title should say 'XtremeDrive'", () => {
    debugEl = fixture.debugElement.query(By.css("ion-title"));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain("XtremeDrive");
  });


  
});
