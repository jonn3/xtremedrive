import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryPopoverPageRoutingModule } from './history-popover-routing.module';

import { HistoryPopoverPage } from './history-popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryPopoverPageRoutingModule
  ],
  declarations: [HistoryPopoverPage]
})
export class HistoryPopoverPageModule {}
