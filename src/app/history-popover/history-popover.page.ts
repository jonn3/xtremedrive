import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-history-popover',
  templateUrl: './history-popover.page.html',
  styleUrls: ['./history-popover.page.scss'],
})
export class HistoryPopoverPage implements OnInit {

  constructor(private popover:PopoverController) { }

  ngOnInit() {
  }

  ClosePopover() {
    this.popover.dismiss();
  }

}
