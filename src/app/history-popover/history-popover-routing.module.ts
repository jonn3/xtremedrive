import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoryPopoverPage } from './history-popover.page';

const routes: Routes = [
  {
    path: '',
    component: HistoryPopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryPopoverPageRoutingModule {}
