import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PopovercomponentPageModule } from './popovercomponent/popovercomponent.module';
import { HelpPageModule } from './help/help.module';
import { AnswerOfferPageModule } from './answer-offer/answer-offer.module';
import { AnswerRequestPageModule } from './answer-request/answer-request.module';
import {HistoryPopoverPageModule} from './history-popover/history-popover.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, PopovercomponentPageModule, HelpPageModule, AnswerOfferPageModule, AnswerRequestPageModule, HistoryPopoverPageModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
