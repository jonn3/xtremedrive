import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnswerOfferPageRoutingModule } from './answer-offer-routing.module';

import { AnswerOfferPage } from './answer-offer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnswerOfferPageRoutingModule
  ],
  declarations: [AnswerOfferPage]
})
export class AnswerOfferPageModule {}
