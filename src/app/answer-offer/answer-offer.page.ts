import { Component, OnInit } from '@angular/core';
import { PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-answer-offer',
  templateUrl: './answer-offer.page.html',
  styleUrls: ['./answer-offer.page.scss'],
})
export class AnswerOfferPage implements OnInit {

  constructor(private popover:PopoverController) { }

  ngOnInit() {
  }

  ClosePopover() {
    this.popover.dismiss();
  }

}
