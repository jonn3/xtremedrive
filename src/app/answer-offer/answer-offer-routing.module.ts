import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnswerOfferPage } from './answer-offer.page';

const routes: Routes = [
  {
    path: '',
    component: AnswerOfferPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnswerOfferPageRoutingModule {}
