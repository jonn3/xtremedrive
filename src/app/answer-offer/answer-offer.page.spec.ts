import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnswerOfferPage } from './answer-offer.page';

describe('AnswerOfferPage', () => {
  let component: AnswerOfferPage;
  let fixture: ComponentFixture<AnswerOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerOfferPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnswerOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
