import { TestBed } from '@angular/core/testing';

import { Rides2Service } from './rides2.service';

describe('Rides2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Rides2Service = TestBed.get(Rides2Service);
    expect(service).toBeTruthy();
  });
});
