export class Ride {
    id: number;
    kunta: string;
    kaupunginosa: string;
    osoite: string;
    kohde: string;
    paiva: string;
    aika: string;

  
 

    constructor(newId: number, newKunta: string, newKaupunginosa: string, newOsoite: string,
        newKohde: string, newPaiva: string, newAika: string,) {
        this.id = newId;
        this.kunta = newKunta;
        this.kaupunginosa = newKaupunginosa;
        this.osoite = newOsoite;
        this.kohde = newKohde;
        this.paiva = newPaiva;
        this.aika = newAika;
 
    }
}