import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {HistoryPopoverPage} from '../history-popover/history-popover.page';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  constructor(private popover:PopoverController) { }

  ngOnInit() {
  }

    CreatePopover() {
      this.popover.create({component:HistoryPopoverPage,showBackdrop:false}).then((popoverElement)=>{
        popoverElement.present();
      })
    }
}
