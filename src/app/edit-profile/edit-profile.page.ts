import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

name: string;
prof: string;
phone: number;
email: string;
address: string;

name2: string;
prof2: string;
phone2: number;
email2: string;
address2: string;

  constructor(private router: Router) { }

  ngOnInit() {

    this.name = "Pena Hulvaton";
    this.prof = "Luonnontieteiden lehtori";
    this.phone = 4013371337;
    this.email = "stout.shako4@tworefined.tf";
    this.address = "Molluskinkatu 44";
  }

  back() {
    this.router.navigateByUrl('options');
  }

}
