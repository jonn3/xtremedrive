import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import {
  async,
  ComponentFixture,
  TestBed,
  inject,
} from "@angular/core/testing";

import { IonicModule } from '@ionic/angular';

import { PopovercomponentPage } from './popovercomponent.page';

import { debug } from 'util';

import { By } from '@angular/platform-browser';

import { HttpClientModule } from "@angular/common/http";

describe('PopovercomponentPage', () => {
  let component: PopovercomponentPage;
  let fixture: ComponentFixture<PopovercomponentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopovercomponentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopovercomponentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('PopovercomponentPage', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
      const service: PopovercomponentPage = TestBed.get(PopovercomponentPage);
      expect(service).toBeTruthy();
    });

  });
  
});
