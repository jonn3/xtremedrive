export class Ride2 {
    id2: number;
    kunta2: string;
    kaupunginosa2: string;
    osoite2: string;
    kohde2: string;
    paiva2: string;
    aika2: string;

  
 

    constructor(newId2: number, newKunta2: string, newKaupunginosa2: string, newOsoite2: string,
        newKohde2: string, newPaiva2: string, newAika2: string,) {
        this.id2 = newId2;
        this.kunta2 = newKunta2;
        this.kaupunginosa2 = newKaupunginosa2;
        this.osoite2 = newOsoite2;
        this.kohde2 = newKohde2;
        this.paiva2 = newPaiva2;
        this.aika2 = newAika2;
 
    }
}