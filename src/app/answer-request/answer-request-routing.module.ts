import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnswerRequestPage } from './answer-request.page';

const routes: Routes = [
  {
    path: '',
    component: AnswerRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnswerRequestPageRoutingModule {}
