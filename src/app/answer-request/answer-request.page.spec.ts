import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnswerRequestPage } from './answer-request.page';

describe('AnswerRequestPage', () => {
  let component: AnswerRequestPage;
  let fixture: ComponentFixture<AnswerRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerRequestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnswerRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
