import { Component, OnInit } from '@angular/core';
import { PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-answer-request',
  templateUrl: './answer-request.page.html',
  styleUrls: ['./answer-request.page.scss'],
})
export class AnswerRequestPage implements OnInit {

  constructor(private popover:PopoverController) { }

  ngOnInit() {
  }

  ClosePopover() {
    this.popover.dismiss();
  }

}
