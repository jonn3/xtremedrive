import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnswerRequestPageRoutingModule } from './answer-request-routing.module';

import { AnswerRequestPage } from './answer-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnswerRequestPageRoutingModule
  ],
  declarations: [AnswerRequestPage]
})
export class AnswerRequestPageModule {}
