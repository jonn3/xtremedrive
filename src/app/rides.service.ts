import { Injectable } from '@angular/core';
import { Ride } from './ride';

@Injectable({
  providedIn: 'root'
})
export class RidesService {
  offer = Array<Ride>();
  
  


  constructor() {



    this.offer.push(new Ride(4, 'Tyrnävältä', '', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));
    this.offer.push(new Ride(5, 'Kempeleestä', '', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));
    this.offer.push(new Ride(6, 'Limingasta', '', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));
    this.offer.push(new Ride(7, 'Oulusta', '', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));
   }

   getElement(id: number) {
     for (const td of this.offer) {
       if (td.id === id) {
         return td;
       }
     }

   }



   addRide(kunta: string, kaupunginosa: string, osoite: string, kohde: string, paiva: string, aika: string,) {
     const id: number = this.offer.length;
     this.offer.push(new Ride(id, kunta, kaupunginosa, osoite, kohde, paiva, aika));
   }
}