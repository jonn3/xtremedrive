import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import {
  async,
  ComponentFixture,
  TestBed,
  inject,
} from "@angular/core/testing";


import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';


import { debug } from 'util';

import { By } from '@angular/platform-browser';

import { HttpClientModule } from "@angular/common/http";

describe('AppComponent', () => {

  let statusBarSpy, splashScreenSpy, platformReadySpy, platformSpy;
  let debugEl: DebugElement;
  let htmlEl: HTMLElement;
  let fixture: ComponentFixture<AppComponent>;


  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: StatusBar, useValue: statusBarSpy },
        { provide: SplashScreen, useValue: splashScreenSpy },
        { provide: Platform, useValue: platformSpy },
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  // TODO: add more tests!

  it('should have "Etusivu" in menu', () => {
    debugEl = fixture.debugElement.query(By.css('ion-list'));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain('Etusivu');
  });

});
