import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { PopoverController} from '@ionic/angular';
import { HelpPage } from './help/help.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  navigate: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private popover:PopoverController
  ) {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title : "Etusivu",
        url   : "/home",
        icon  : "home"
      },
      {
        title : "Historia",
        url   : "/history",
        icon  : "albums"
      },
      {
        title : "Asetukset",
        url   : "/options",
        icon  : "settings"
      },
      {
        title : "Kirjaudu ulos",
        url   : "/login",
        icon  : "log-out"
      },
    ]
  }

  CreatePopover() {
    this.popover.create({component: HelpPage,
    showBackdrop:false}).then((popoverElement)=>{
      popoverElement.present();
    });
  }  

}
