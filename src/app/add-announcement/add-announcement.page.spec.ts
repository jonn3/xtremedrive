import { IonicModule } from '@ionic/angular';

import { AddAnnouncementPage } from './add-announcement.page';

import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import {
  async,
  ComponentFixture,
  TestBed,
  inject,
} from "@angular/core/testing";

import { debug } from 'util';

import { By } from '@angular/platform-browser';

import { HttpClientModule } from "@angular/common/http";

describe('AddAnnouncementPage', () => {
  let component: AddAnnouncementPage;
  let fixture: ComponentFixture<AddAnnouncementPage>;
  let debugEl: DebugElement;
  let htmlEl: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAnnouncementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddAnnouncementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));


  it("should display the back button", () => {
    debugEl = fixture.debugElement.query(By.css("ion-back-button"));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain;
});

});
