import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RidesService } from '../rides.service';

@Component({
  selector: 'app-add-announcement',
  templateUrl: './add-announcement.page.html',
  styleUrls: ['./add-announcement.page.scss'],
})
export class AddAnnouncementPage implements OnInit {
  id: number;
  kunta: string;
  kaupunginosa: string;
  osoite: string;
  kohde: string;
  paiva: string;
  aika: string;

 

  constructor(public router: Router, public ridesService: RidesService) { }

  ngOnInit() {
  }

  save() {
    this.ridesService.addRide(this.kunta, this.kaupunginosa, this.osoite, this.kohde, this.paiva, this.aika,);
    this.router.navigateByUrl('/home');
  }

  close() {
    this.router.navigateByUrl('/home');
  }

}
