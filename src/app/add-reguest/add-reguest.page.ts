import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Rides2Service } from '../rides2.service';

@Component({
  selector: 'app-add-reguest',
  templateUrl: './add-reguest.page.html',
  styleUrls: ['./add-reguest.page.scss'],
})
export class AddReguestPage implements OnInit {
  id2: number;
  kunta2: string;
  kaupunginosa2: string;
  osoite2: string;
  kohde2: string;
  paiva2: string;
  aika2: string;

  constructor(public router: Router, public rides2Service: Rides2Service) { }

  ngOnInit() {
  }

  save2() {
    this.rides2Service.addRide2(this.kunta2, this.kaupunginosa2, this.osoite2, this.kohde2, this.paiva2, this.aika2,);
    this.router.navigateByUrl('/home');
  }

  close2() {
    this.router.navigateByUrl('/home');
  }

}
