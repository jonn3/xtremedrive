import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddReguestPageRoutingModule } from './add-reguest-routing.module';

import { AddReguestPage } from './add-reguest.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddReguestPageRoutingModule
  ],
  declarations: [AddReguestPage]
})
export class AddReguestPageModule {}
