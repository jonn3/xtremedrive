import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddReguestPage } from './add-reguest.page';

describe('AddReguestPage', () => {
  let component: AddReguestPage;
  let fixture: ComponentFixture<AddReguestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReguestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddReguestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
