import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddReguestPage } from './add-reguest.page';

const routes: Routes = [
  {
    path: '',
    component: AddReguestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddReguestPageRoutingModule {}
