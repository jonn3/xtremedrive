import { Component, OnInit } from '@angular/core';
import { PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  constructor(private popover:PopoverController) { }

  ngOnInit() {
  }

  ClosePopover() {
    this.popover.dismiss();
  }

}
