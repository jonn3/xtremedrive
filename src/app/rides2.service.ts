import { Injectable } from '@angular/core';
import { Ride2 } from './ride2';

@Injectable({
  providedIn: 'root'
})
export class Rides2Service {
  request = Array<Ride2>();
  


  constructor() {
    this.request.push(new Ride2(1, 'Kempele', 'Hiekkala', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));
    this.request.push(new Ride2(2, 'Oulu', 'Tuira', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));
    this.request.push(new Ride2(3, 'Liminka', 'Päämaa', '', 'Linnanmaalle', 'TÄNÄÄN', '08.30',));


  
   }

   getElement2(id2: number) {
     for (const ip2 of this.request) {
       if (ip2.id2 === id2) {
         console.log(ip2)
         return ip2;
       }
     }


   }



   addRide2(kunta2: string, kaupunginosa2: string, osoite2: string, kohde2: string, paiva2: string, aika2: string,) {
     const id2: number = this.request.length;
     this.request.push(new Ride2(id2, kunta2, kaupunginosa2, osoite2, kohde2, paiva2, aika2));
   }
}